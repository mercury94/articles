# Матрицы в Android 

### Актуальность темы
  Если вы разрабатывваете приложения под ОС Android, то, время от времени, сталкиваетесь с нетривиальными задачами. Например, дизайнер выдал макет с нестандартыми элементами представления. И вы, прикинув сложность реализации, начинаете искать библиотеки, которые уже делают то, что вам нужно. В лучшем случае, вы находите, затем интегрируете с наименьшими усилиями, все довольны, и все счастливы. Бывает и так, что вы находите приблизительное решение вашей задачи, но, чтобы получить желаемый результат, нужно внести изменения в существующее решение, но тогда есть вероятность фиксить баги в библиотеке. В наихудшем случае вы не находите ничего похожего на элемент представления из макета. Вы объясняете дизайнеру, что писать с нуля эту вьюшку затратно по времени и просите заменить на аналогичное решение, полегче. В ответ от дизайнера вы слышите, что это - ключевой элемент дизайна, и заменить его нельзя, так как это нарушит общую концепцию дизайна и т.д. Ну что-же, смирившись со своей участью, начинате искать способы решения. 

### Введение
На протяжении трёх статей я попытаюсь раскрыть и объяснить основные принципы трансформаций двухмерного пространства с помощью Аффинных преобразований, а также расмотрим примеры часто используемых трансформаций. Для демонстрации примеров я буду использовать компонент ImageView c установленым параметром ScaleType = MATRIX. Отметим, что все трансформации применяемые к ImageView так же могут быть применены к вашим кастомным view - элементам, если вдруг для отрисовки вы используете шейдеры.

Хорошо, а теперь расмотрим какие темы будут расматриваться во всех частях статьи:

  * Афинные преобразования (1 часть);;
  * Матрицы в Android (1 часть);
  * Post и Pre функции трансформаций в Android (1 часть);
  * Перемещение (Transition) (1 часть);
  * Масштабирование (Scale) (2 часть);
  * Поворот (Rotate) (2 часть);
  * Наклон (Skew) (3 часть) ;
  * Отражение (Sharing) (3 часть);

Каждый раздел преобразования включает в себя примеры, на которых подробно рассматриваются изменения плоскости объекта.

Чего вы не найдете в данной статье, так это работу с изображением в 3D пространстве.

Прежде чем перейти к основной теме обсуждения, предлагаю вам перейти по [ссылке](http://math1.ru/education.html#matrix) и освежить в памяти (или ознакомиться) с матрицами и операциями над ними.

### Аффинные преобразования 

  Прежде чем перейдем к обсуждению аффиных преобразований. Нужно понять несколько моментов. Прежде чем изображение появится на экране, оно должно быть как-то описанно  в графической системе устройства. Первоначально свойства объекта описываются в системе координат этого самого объекта. Это приводит к тому, что, при изменении пространтсва, объект остается неизменным с течением времени. Например, на экране находится 2 разные картинки, и каждая из них будет описываться в своей системе координат, и преобразование плоскости одной картинки не влечет изменение другой. Таким образом, введем новое понятие: мировая система координат, где располагаются все объекты.
  Пользователи, по своей сути, являются наблюдателями и, так же, живут в своей системе координат (система координат наблюдателя). Системы координат объектов и наблюдателя являются трехмерными. Получается, в процессе обработки изображения, система координат объекта преобразуется в двумерную координатную плоскость. 
  Объекты могут эволюционировать относительно наблюдателя. То есть менять свое расположение, сжиматься, вращаться и т.д. Есть два решения для описания этого процеса. Первый подход предпологает, что система координат наблюдателя неподвижена, а мировая система координат и система координат объекта могут эволюционировать. Таким образом, удобно наблюдать изменения объекта во времени. Второй подход предпологает, что неподвижна мировая система координат, а системы координат наблюдателя и объекта эволюционируют. В операционной системе андройд используется первый подход. 
    Сейчас важно было понять, что имеется 2 плоскости:
  1. Плоскость объекта, над котороый мы будем производить преобразования;
  2. Плоскость наблюдателя, благодаря которой мы можем наблюдать изменения объекта;
  
  Вся работа с объектами происходит в прямогульной декартовой системе координат. Каждая точка в данной системе описывается парой координат x и y. Процесс аффинного преобразования точки является переносом данной точки на другую (совмещенную) систему координат. Это процесс описывается системой уравнений:

![alt text](https://bitbucket.org/mercury94/articles/raw/15c5af01e3e04b3d86e7d01896cfc58a7faea322/formuls.png) 

  Здесь:
      x, y - координаты точки в "старой" системе координат;
      x', y' - координаты точки в "новой" системе координат;
      x(0)',y(0)' - расстояние сдвига одной системы координат по оси абцисс и ординат соответсвтенно;
      a(ij) - числа описывающие параметры преобразования, и они связаны неравенством:
  
  ![alt text](https://bitbucket.org/mercury94/articles/raw/15c5af01e3e04b3d86e7d01896cfc58a7faea322/matrix_2d.png) 

Справедлива будет следующая запись:

![alt text](https://bitbucket.org/mercury94/articles/raw/ad6f8f7d3acd416098a4af390e2bc32121378b0b/%20matrix_2d_2.png)

Но матрица 2 x 2 имеет следующие проблемы:
 - ограничена в количестве выполняемых операций ;
 Данная матрица может выполнять операции: масштабирвоание, вращение, отражение, наклон. Но, к сожалению, мы не сможем выполнить смещение; 
 - Трудоемкий процесс расчета композиции преобразований. Например, композиция из одного преобразования уже выглядит не очень хорошо, а на практике выполняются десятки преобразований:

![alt text](https://bitbucket.org/mercury94/articles/raw/bc0e8ec56d5ebd268304af55e7a2293b51b295ea/matrix2x2_multi.png)

Помимо того, что это сложно считать, вызывает затруднение и запись, когда композиция состоит из нескольких преобразований. 

В таком случае, было принято решение ввести еще одну координату (z). То есть, мы погружаем наше двумерное пространство в трехмерное. Так мы попадаем в однородную систему координат.
С математической точки зрения, однородным представлением n-мерного объекта называют его представление в (n+1)-мерном пространтсве, полученного добавлением еще одного множителя, так называемого, скалярного множителя.
  Итак, чтобы найти однородные координаты точки, необходимо выполнить следующее.
Предположим, что на плоскости лежит точка T с координатами  x и y.

![alt text](https://bitbucket.org/mercury94/articles/raw/8b9bca0f35e81001ee91285a06a44a8d4e24e715/image1.png)

Однородными координатами данной точки называется любая тройка одновременно не равных нулю чисел(w1, w2, w3), которые связаны с координатами точки T следующими соотношениями:

![alt text](https://bitbucket.org/mercury94/articles/raw/a50de0db2a8ff93922800752752406b682c120f1/x%3Dw1w3.png)

![alt text](https://bitbucket.org/mercury94/articles/raw/a50de0db2a8ff93922800752752406b682c120f1/y%3Dw2w3.png)

Точку T можно представить тройкой своих координат w1,w2,w3: любая выбранная точка лежащая на прямой, котороя соединяет начало координат O(0,0,0), c точкой T' (x,y,1).
Очевидино, что точка T' однозначно определяет точку T, также как и любая точка T'' с координатами (xh,yh,h). Здесь h - является скалярным множителем. Мы сейчас описали точку в однородных координатах. Обычно точку представляют как (x: y: 1). То есть h = 1. Тем не менее, используется общая форма записи (w1 : w2 : w3). Так что, пусть вас такая запись не смущает, если, вдруг, где-нибудь встретите. 
Как вы знаете, существует и декартова система пространства, где точки также описываются тройкой координат (x,y,z). И, чтобы отличить однородное описание точки от привычного декартовского описания, в однородном описании координаты точки разделяют не запятой, а двоеточием. Итак, теперь мы можем записать общую формулу аффиного преобразования. 

![alt text](https://bitbucket.org/mercury94/articles/raw/cc7a05bd54dd320ea30b045d4905dd91e9776847/matrix_af.png)

При умножении этих матрицы, мы получим два основных уравнения, которые описывают перенос точки от одной системы координат в совмещенную.

Аффиные преобразования являются линейными. Правила, которым подчинаются линейные преобразования:
  * Прямые (плоскости) переходят в прямые (плоскости);
  * Пересекающиеся прямые (плоскости) в пересекающиеся;
  * Параллельные в параллельные.

Мы теперь знаем, что для двумерных однородных координат используется матрица линнейного преобразования общего вида. 

![alt text](https://bitbucket.org/mercury94/articles/raw/228a85077be747ab552a45601c3c7ecc36136422/matrix_at_3x3.png) 
    
Данная матрица содержит 4 подматрицы. Которые составлены по принципу разнородности их влияния на результат линейного преобразования. 

  1.   Ниже изображена подматрица 2 х 2. 
![alt text](https://bitbucket.org/mercury94/articles/raw/5bb624ffff8c6a837f50e29401121951a0f6db22/submatrix_2x2_1.png) 

  Элементы данной подматрицы отвечают за операции: масштабирование, вращения и сдвиг;

  2. Следующая подматрица 1 х 2. 
![alt text](https://bitbucket.org/mercury94/articles/raw/5bb624ffff8c6a837f50e29401121951a0f6db22/submatrix_1x2_2.png) 
     Элементы данной подматрица отвечают за операцию перемещения (смещения).
    
  3. Подматрица 2 х 1.
      ![alt text](https://bitbucket.org/mercury94/articles/raw/5bb624ffff8c6a837f50e29401121951a0f6db22/submatrix_2x1_3.png) 
      Элементы данной подматрицы отвечают за задание проекции. 
  4. И наконец подматрица размерностью 1 x 1.
      ![alt text](https://bitbucket.org/mercury94/articles/raw/5bb624ffff8c6a837f50e29401121951a0f6db22/submatrix_1x1_4.png) 
      Элемент данной подматрицы отвечает за однородное изменение масштаба.

  По своей сути это - элементарные преобразования. Более сложные преобразования представляют цепочку последовательно выполняемых элементарных преобразований.

   В конечном итоге, мы ищем положение точки после выполнения операцией преобразования над ней:

   ![alt text](https://bitbucket.org/mercury94/articles/raw/f7bdf795f63b4936482c466dc46d0308756fdee1/matrix_at_basic.png)

   Здесь:
   T - применяемая матрица преобразования;
   x, y - координаты точки в исходной вспомогательном снимке;
   x', y' - координаты точки на совмещеном вспомогательном снимке;
   z, z' - координаты точки на исходном и совмещеном вспомогательном снимке; 
   
   
### Матрицы в Android 

В документации SDK для андройда, по моему мнению, содержится довольно скудная инфомация про класс Matrix. И не содержится абсолютно никакой информации про аффинные преобразования. То есть разработчики изначально предполагают, что если вы пишите приложения под Android, то должны знать как выполняются Аффинные преобразования. К сожалению, чаще всего это не так.
Если взглянуть на класс матрицы в андройде [android.graphics.Matrix](https://android.googlesource.com/platform/frameworks/base/+/master/graphics/java/android/graphics/Matrix.java), то можно увидеть много oops(); и вызовов методов native_. В общем, никакой полезной информации там нет. Это потому, что реальные операции выполняются классом Matrix, написанном на C++. То есть, Matrix.java является прослойкой между пользовательским кодом и нативными вызовами. 
Предполгаю, что кого-нибудь заинтересует реализция класса Matrix.cpp. 
[Здесь](https://android.googlesource.com/platform/frameworks/base/+/master/core/jni/android/graphics/Matrix.h) вы найдете интерфейс Matrix.h, а [здесь](https://android.googlesource.com/platform/frameworks/base/+/master/core/jni/android/graphics/Matrix.cpp) сообственно реализацию Matrix.cpp. 
Но и это не все. Matrix.cpp также, в свою очередь, является посредником, так как все операции над матрицами он делегирует классу SkMatrix. Интерфейс этого класса можете глянуть [здесь](https://android.googlesource.com/platform/external/skia/+/master/include/core/SkMatrix.h) (SkMatrix.h) и его реализацию [здесь](https://android.googlesource.com/platform/external/skia/+/master/src/core/SkMatrix.cpp) (SkMatrix.cpp). 
    
  Если мы начнем изучать класс матриц, со страницы  документации от гугл ([ссылка](https://developer.android.com/reference/android/graphics/Matrix)). Первое, что нам показывают - это список констант этого класса. Каждая константа является индексом в массиве который описывает матрицу. Видно, что имеются методы геттер и сеттер. Так как матрицы 3 x 3, то размер оперируемого массива = 9.
  Смысл параметров массива мы рассмотри чуть позже, а сейчас предлагаю ознакомиться с основными методами класса Matirx.java.  
    
### Post и Pre функции трансформаций в Android

Предполагаю, что вы кратенько изучили документацию от гугл по классу матриц.
Скорее всего вы заметили, что мы можем выполнить различные операции преобразования несколькими способами. А именно, используются методы post и pre:

Post-методы:

~~~ java

  ...
  postTranslate(float dx, float dy) 
  postScale(float sx, float sy)
  postScale(float sx, float sy, float px, float py)
  postRotate(float degrees)
  postRotate(float degrees, float px, float py)
  postSkew(float kx, float ky)
  postSkew(float kx, float ky, float px, float py)
  postConcat(Matrix other)
  ...
    
~~~

Pre-методы:
~~~ java

  ...
  preTranslate(float dx, float dy) 
  preScale(float sx, float sy)
  preScale(float sx, float sy, float px, float py)
  preRotate(float degrees)
  preRotate(float degrees, float px, float py)
  preSkew(float kx, float ky)
  preSkew(float kx, float ky, float px, float py)
  preConcat(Matrix other)
  ... 
    
~~~

Из документации методов видим порядок умножения матриц меняется. В случае с post-методами: 

![](https://bitbucket.org/mercury94/articles/raw/6d734f58df0f2805bbbf10d5285eb6bbca0eae52/matrix_post.png)

И pre-методами:

![](https://bitbucket.org/mercury94/articles/raw/6d734f58df0f2805bbbf10d5285eb6bbca0eae52/matrix_pre.png)

Где: 
M' - новая матрица;
T - матрица преобразования;
M - старая матрица;

Мы знаем, что одно из свойств умножение матриц это не коммутативность, т.е.:

![](https://bitbucket.org/mercury94/articles/raw/01cb1ff60daa0ee82e9e137d3b7a608b03ebab8b/AB.png)

А это означает, что при выполнении pre- и post- опреации мы получим различные результаты.

Из коробки мы имеет достаточно широкий арсенал для управления матрицами. С помощью методов postConcate и preConcate вы можете задать свое преобразование, которого не предусмотрели разработчики (Например, отражение). 

 > В стандартном JDK тоже есть классы для управления матрицами и выполнения аффинных преобразований. Также там есть аналогичные методы для умножения матриц. Один из таких методов preConcatenate(...), который является аналогом preConcat(...). Но важно знать что в JDK порядок умножения матриц разный. То есть при выполнении любой pre-операции, формула умножения такая: M' = T * M. Мне этот момент кажется достаточно странным. 


### Подготовка ресурсов

Прежде чем мы перейдем в обсуждению преобразований, нам необходимо подготовить рабочую среду, в которой будем проводить свои тесты. Первым делом, я скачал картинку в разрешении 240x320 пикселей.
  
  ![](https://images.wallpaperscraft.ru/image/kot_morda_ochki_tolstyy_65455_240x320.jpg)
  
 Создал новый пустой проект, в котором описал activity_main (Layout):
  
~~~ xml

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android" 
              xmlns:app="http://schemas.android.com/apk/res-auto"
              xmlns:tools="http://schemas.android.com/tools"
              android:layout_width="match_parent"
              android:layout_height="match_parent"
              tools:context=".MainActivity">

<ImageView
    android:id="@+id/imageView"
    android:src="@drawable/image"
    android:scaleType="matrix"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />

</LinearLayout>

~~~

Размеcтил ImageView по всей доступной родительской площади. И, чтобы я мог выполнять аффинные операции над выбранным изображеним, установил поле scaleType в значение matrix. 

  В своих преобразованиях я буду работать с размерами изображений (не во всех). Ширину и высоту картинки мы можем получить с помощью методов getIntrinsicWidth() и getIntrinsicHeight() соответственно у экземпляра класса Drawable. Важно понимать, что эти методы не всегда возвращают точные размеры изображения. Это происходит потому, что растровое изображение лежит в неправильной папке drawable. Если вы хотите получать точные размеры изображения, вам необходимо позаботиться о наличии изображений, подходящих для различных плотностей пикселей, и размещении их в соответствующих папках drawable. Подробнее можете ознакомиться в данной [статье](https://developer.android.com/training/multiscreen/screendensities?hl=ru). 
  Я буду тестировать на эмуляторе устройства Nexus 4. Учитывая плотность пикселей, я разместил изображение в папке drawable-xhdpi с именем image. Так я буду получать точные размеры изображения.

В исходном состоянии левый верхний угол изображения соответствует центру координатных осей, матрица координат картинки получается следующая:
  
  ![Alt Text](https://bitbucket.org/mercury94/articles/raw/bd2affed0c8697d3681976c51303ad99a7553b80/matrix_input_coords.png)

Вот так выглядит начальное изображение без преобразований:

![alt text](https://bitbucket.org/mercury94/articles/raw/65beb70f413e0d70bc1098f301f89f36d7e9f9f0/sample_1_before.png)
 
 Теперь нам необходимо получить иходную матрицу изображения. Для этого напишем метод, который будет выводит в лог параметры переданной матрицы в параметрах.
 
~~~ java

package mercuriy94.com.matrix.affinetransformations;

//Импорты 
...
  
public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                printMatrixValues(imageView.getImageMatrix());
            }
        });
    }

    private void printMatrixValues(Matrix matrix) {
        float[] values = new float[9];
        matrix.getValues(values);
        Log.i(TAG, String.format("Matrix values:\n" +
                        "%f %f %f\n" +
                        "%f %f %f\n" +
                        "%f %f %f",
                values[Matrix.MSCALE_X], values[Matrix.MSKEW_X], values[Matrix.MTRANS_X],
                values[Matrix.MSKEW_Y], values[Matrix.MSCALE_Y], values[Matrix.MTRANS_Y],
                values[Matrix.MPERSP_0], values[Matrix.MPERSP_1], values[Matrix.MPERSP_2]));
    }
}

~~~

Вывод:

![](https://bitbucket.org/mercury94/articles/raw/4567cdd1acfb77e1773542cfb1aee11f9edf1d54/matix_values_log_input.png)
  
 Центр координатной оси наблюдателя расположен в верхнем левом углу контейнера ImageView. 
Обратите внимание , что исходная матрица является единичной,  а это означает, что ее можно не учитывать в вычислениях, так как она не оказывает влияние на результат:
  
  ![Alt Text](https://bitbucket.org/mercury94/articles/raw/f6348fb842d1ab4df8dfb3551fad95e91dbc1edb/android_input_matrix.png)
  
 Метод `printMatrixValues(Matrix matrix) ` нам еще пригодится для вывода в лог окончательной матрицы преобразований.
 
Обратите внимание, что все преобразования сводятся к умножению точки лежащей на плоскости на матрицу преобразования.  Реультатом являются координаты совмещенной точки. В таком случае необходимо добавить функционал, который будет подтверждать или опровергать правильность вычислений, то есть выводить в лог координаты точек совмещенного изображения. Класс Matrix из SDK Android нам не подходит, так как он работет только с матрицами 3x3. Нам как минимум требуется матрица размерностью 3x4, для расчета координат углов изображеня. Подключаем эффективную бибилотеку для работы с матрицами ([Ссылка на главную страницу](http://ejml.org/wiki/index.php?title=Main_Page)). 
Обратите внимание, что данная бибилотека требует использование JDK 8. Переходим по [Ссылке](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)  и скачиваем JDK 8 для вашей ОС. Далее  необходимо установить скаченный файл. Установака тривиальная и не требует комментариев по настройке.
  После установки, переходим в структуру проекта (File->Project Structure...). Открываем вкладку SDK Location, и в разделе JDK location убираем галочку c параметра "Use embedded JDK (recomended). Теперь нам доступен выбор jdk для проекта. Кликаем по многоточию справа от поля где указывается путь до jdk и выбираем путь до установленного нами jdk.

![](https://bitbucket.org/mercury94/articles/raw/801928c49419630879772bad2269ce3fc89735b2/img_project_structure.png)
 
Отредактируем файл конфиуграции gradle. 

~~~ Gradle
 
...

android {

...

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

dependencies {
 
 ...
 
    implementation group: 'org.ejml', name: 'ejml-all', version: '0.34'
    
....

}

...
 
~~~
 
 Вот теперь мы можем с легкостью написать метод который сможет нам показать координаты изобаржения после применения преобразования:
 
~~~ java

...

  import org.ejml.simple.SimpleMatrix;

...

    private void printImageCoords(Matrix matrix) {

        float imageWidth = imageView.getDrawable().getIntrinsicWidth();
        float imageHeight = imageView.getDrawable().getIntrinsicHeight();

        float[] inputCoords = new float[]{
                0f, imageWidth, imageWidth, 0f,
                imageHeight, imageHeight, 0f, 0f,
                1f, 1f, 1f, 1f};

        SimpleMatrix matrixCoords = new SimpleMatrix(3, 4, true, inputCoords);
        matrixCoords.print();

        float[] values = new float[9];
        matrix.getValues(values);
        SimpleMatrix imageImatrix = new SimpleMatrix(3, 3, true, values);
        SimpleMatrix resultCoords = imageImatrix.mult(matrixCoords);
        resultCoords.print();
    }

...

~~~
 
 Мне кажется данный метод треубет комментариев, по этому давайте его разберем. 
 В качестве параметра передается окончательная матрица преобразования. Далее получаем размеры изображения и на их основе создаем исходную матрицу координат. С этим нам помогает класс SimpleMatrix.
У данного класса имеется несколько конструкторов. Вы можете более детально изучить данный класс перейдя по  [ссылке](http://ejml.org/javadoc/). А мы остановимся на выбранном конструкторе. 
В качестве первых двух параметров мы передаем кол-во строк и стобцов соответственно, из которых будет состоять наша матрица. 
Третьим параметром передаем true, так как наша матрица является закодированной в строчный массив, то есть одномерный массив. И последним четвертым параметром сам массив значений матрицы.  
  Далее выполняем перевод матрицы переданной в параметрах в удобный для расчета класс SimpleMatrix. 
Умножение матрицы выполняется  методом  `mult` из класса SimpleMatrix, результатом которого является новая результирующая матрица. 
Данный класс умеет самостоятельно выводить значения с помощью методов `print()`. Вывод попадает  в Logcat c тегом = "System.out", что добавляет нам удобство  в его чтении. 
Ниже представлен результат коодинат исходной матрицы:

![](https://bitbucket.org/mercury94/articles/raw/e1de4bd73db5b8447d09f21b9a1f46dc34cb75da/sample_result_coords_img.png)
 
Теперь, когда у нас все готово, можем приступить к рассмотрению преобразований!

### Перемещение (Сдвиг) (Transition)

  По моему мнению, это - самый простой вид преобразования, его также называют параллельный перенос.
  Преобразование сдвига  устанавливает соответствие между координатами точки в двух координатных системах, одна из которых сдвинута относительно другой на расстояние дельта X по горизонтали и дельта Y по вертикали.

![alt text](https://bitbucket.org/mercury94/articles/raw/2c508e4f5d74f8bc79cc503a68b9d1896fec94b7/formula_trans.png)

Если начать раскрывать скобки, то получим следующую систему уравнений:

![alt text](https://bitbucket.org/mercury94/articles/raw/f15a5cd8c0964a08f8a6312aa5bce32c50065a54/formula_trans_2.png)

Чтобы решить такую систему, нам необходимо рассмотреть каждое равенство в отдельности. Начнем с первого:

![alt text](https://bitbucket.org/mercury94/articles/raw/f15a5cd8c0964a08f8a6312aa5bce32c50065a54/formula_trans_x.png)

Нам необходимо выбрать такие значение коэффициентов a,b и с, чтобы данное равенство было верно.
Мы с вами уже знаем, что z всегда равна 1, это обсуждалось в разделе "Аффинные преобразования". 
Так как левая часть уравнения не содержит параметра y, следовательно b = 0. Давайте взглянем на  получившееся уравнение:

![alt text](https://bitbucket.org/mercury94/articles/raw/41aad2e6e91a16cce03fdb81c6f1894320250bc9/formula_trans_x_2.png)

Теперь остается подобрать коэффициенты a и с. Следовательно:

![alt text](https://bitbucket.org/mercury94/articles/raw/a68e654566ef6436782100ad3ec9fb2756b24d29/a%3D1%20c%3Dtx.png)

Теперь перейдем ко второму уравнению:

![alt text](https://bitbucket.org/mercury94/articles/raw/f15a5cd8c0964a08f8a6312aa5bce32c50065a54/formula_trans_y.png)

Здесь, как и в первом уравнении нужно найти коэффициенты d,e и f для выполнения данного равенства.
Знаем, что z = 1. Левая часть не содержит параметра x, значит d = 0.
Получаем уравнение:
 
 ![alt text](https://bitbucket.org/mercury94/articles/raw/41aad2e6e91a16cce03fdb81c6f1894320250bc9/formula_trans_y_2.png)
 
Из этого уравнения получаем:

 ![alt text](https://bitbucket.org/mercury94/articles/raw/a68e654566ef6436782100ad3ec9fb2756b24d29/e%3D1%20f%3Dty.png)
 
C третьим уравннием все еще легче. 

![alt text](https://bitbucket.org/mercury94/articles/raw/f15a5cd8c0964a08f8a6312aa5bce32c50065a54/formula_trans_z.png)

Чтобы удовлитврорялось данное равенство, принимаем g и h равные нулю. 
Так как z = 1. То получаем i = 1.

В итоге, для выполнения смещения получаем матрицу размерностью 3 x 3:

![alt text](https://bitbucket.org/mercury94/articles/raw/207e58ab7ce1a823142a1d8a0c81d03d939ccb03/trans_matrix_final.png) 

Скорее всего, если вы до сих пор читатете эту статью, то вам действительно интересно, и все понятно.  Следовательно, я не вижу смысла расписывать каждое преобразование с такой подробностью :=). 

Чтобы понимать, как эту формулу использовать, давайте расмотрим простой пример:
На плоскости размещен квадрат, координаты которого описывает матрица 4 x 2. 

По всей логике, мы должны умножить матрицу координат на матрицу смещения. Но так как размерности матрицы различаются, то мы добавляем к матрице координат строку заполненную единицами. И получаем такую формулу:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/e7d94ff4b67b9881a100bb3f4509b5a589e631e9/formala_matrtix_trans_multiplay.png)

В качестве примера, установим следующие значения (вы можете подставить свои значения):

![Alt Text](https://bitbucket.org/mercury94/articles/raw/f3540c013810bffd74b8f244e92588a206af4b01/TxTy.png)

Подставим все значения, получим следующее:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/f3540c013810bffd74b8f244e92588a206af4b01/matrix_trans_final_example.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/89d6692a26dd1deff7ed163fd866f47b24e39ebc/matrix_trans_emaple.png)

А теперь давайте расмотрим данный тип преобразования на практике.
В качестве примера, сделаем перенос изображения в центр контейнера ImageView, чтобы центр изображения и экрана совпадали. Для того, чтобы вычислить расстояние сдвига по оси абцисс, нам необходимо от ширины контейнера отнять ширину картинки и поделить на 2. Аналогично нужно поступить и с высотой, чтобы вычислить сдвиг по оси ординат. В моем случае ширина контейнера равна 768, а высота 1024. Теперь рассчитаем сдвиги: 

![Alt Text](https://bitbucket.org/mercury94/articles/raw/911004e204229f2c5c76e774d2437e739316c94c/trans_sample_tx_ty_calc.png)

Теперь мы можем вычиcлить конечные координаты углов изображения:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c5a4821acb200f5ebae32ad1ac01655fa2a2de69/sample_trans_results.png)

А вот так будет выглядеть реализация в Android:

~~~ java

package mercuriy94.com.matrix.affinetransformations;

import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                translateToCenter();
            }
        });
    }


    private void translateToCenter() {
        Matrix transformMatrix = new Matrix();

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        float[] matrixValues = new float[]{
                1f, 0f, tx,
                0f, 1f, ty,
                0f, 0f, 1f};

        transformMatrix.setValues(matrixValues);

        Matrix imageMatrix = imageView.getImageMatrix();

        imageMatrix.postConcat(transformMatrix);
        imageView.setImageMatrix(imageMatrix);
        printImageCoords(transformMatrix);
    }
}

~~~

![](https://bitbucket.org/mercury94/articles/raw/65beb70f413e0d70bc1098f301f89f36d7e9f9f0/image_sample_trans_result.png)

Вывод в Logcate:

![](https://bitbucket.org/mercury94/articles/raw/1bd60cb079ac8fa382b2ecb6902ee8c94b50f0ec/matix_values_log_input_trans.png)

Все верно! Рассчитанные и получившиеся коордианты изображения после преобразования совпадают.

Вся соль в методе translateToCenter(), который и выполняет сдвиг в центр контейнера. В данном примере я в ручную заполняю значения матрицы преобразования (matrixValues). Это достаточно утомительное занятие, поэтому в классе matrix есть два метода postTranslate и preTranslate. Различие между post и pre методах обсуждалось ранее в разделе "Post и Pre функции трансформаций в андройде". В качестве параметров, данные методы принимают расстояние сдвига tx и ty. Давайте перепишем метод translateToCenter(), как это выглядело бы в реальной задаче центрирования.

~~~ java

  ...

   private void translateToCenter() {

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        Matrix imageMatrix = imageView.getImageMatrix();

        imageMatrix.postTranslate(tx, ty);
        imageView.setImageMatrix(imageMatrix);
    }

...

~~~

Можете поменять postTranslate на preTranslate, но результат останется таким же. Потому что базовая матрица является единичной. Сообственно, по этому мы не учли ее в вычислениях. Но если бы мы до этого применяли какое-нибудь преобразование, то результаты бы различались.

### Масштабирование изображения (Scale)

Преобразование масштабирования увеличивает или уменьшает размер изображения объекта в системе координат наблюдателя по-сравнению с исходным размером в системе координат этого самого объекта.
  Теперь рассмотрим, по каким принципам происходит масштабирование изображений. Какие тут могут возникать сложности?
Для начала, вы подумаете, и скажите. "Хм..., а можно ведь взять координату точки и умножить на коэффициент масштабирования, и так с другой координатой". Отчасти, вы будете правы. Итак, для начала нам нужно различать коэффициент для каждой оси Sx и Sy, для осей абцисс и ординат соответственно.

В итоге получаем следующее:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/badafb3ce85837b42e4840870d977d18ed72a063/matrix_scale.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/18c664ba4f9bb86d7d1ca7c0c680e3a1ba6a5854/matrix_scale_final.png)

Получим матрицу 3х3 для применения масштабирования:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/46cecd569b0d78ff09c89b7872ead8170826f4a2/matrice_scale.png)

Чтобы применить матрицу масштабирования, ее надо умножить на матрицу координат:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/fa9de303cd85f41e9a4ecbb43a2a493dbc537490/matrix_scale_example.png)

Пример:

У нас имеется квадрат расположенный в Декартовой системе координат, с координатами:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c72d260b4b29e6bad5115b1d2ebfa2c148824c76/matrix_scale_coods.png)

Масштабировать будем в 2 раза:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c72d260b4b29e6bad5115b1d2ebfa2c148824c76/Sx%3D2.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c72d260b4b29e6bad5115b1d2ebfa2c148824c76/Sy%3D2.png)

Из уже известной формулы, вычисляем координаты квадрата в масштабе:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c72d260b4b29e6bad5115b1d2ebfa2c148824c76/matrix_scale_example_conc.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/f635572e697bc6dbbba84ac4bf4895c487a1d163/matrix_scale_example_1.png)

Хорошо, а что насчет масштабирования вокруг опорной точки? Тут нам необходимо выполнить комбинацию преобразований:
1. Сместить плоскость изображение так, чтобы опорная точка совпалдала с началом координат плоскости наблюдателя. Простыми словами, выполнить сдвиг на обратные велечины опорной точки.
2. Применить обычное машстабирование;
3. Выполнить сдвиг плоскости изображения на расстояние координат опорной точки в соответствующих осях.

Тогда матрица 3 х 3 для масштабирваония вокруг опорной точки имеет вид:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/f5f4a398335fa13ad5a7f640b17f200dcaab93fb/matrix_scale_anchor_point_formula.png)

Пример:

В качестве примера, используем начальные координаты квадрата из предыдущего примера, масштаб оставим тоже равным 2 для обеих осей координат. Масштабировать будем вокруг точки с координатами P(5,5), то есть вокруг центра квадрата.

![Alt Text](https://bitbucket.org/mercury94/articles/raw/158799e5d7b3274d568feac4450b8366d99afd1f/Px%3D5.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/158799e5d7b3274d568feac4450b8366d99afd1f/Py%3D5.png)

Подставим уже известные значения и вычислим новые координаты квадрата:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/d1e3c3de2975437b6854d23ed9fa5ee26be3f276/sample_scale_anchor_point_result.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/a7c182ebdad390f188afcea4499141b36b2b810e/matrix_scale_example_2.png)

Видно, что центр квадрата остался на месте относительно начальной оси координат. Данный прием вам скорее всего уже известен из функции Pinch-Zoom. 

Теперь рассмотрим применение масштабирвания в андройде.

В качестве примера растянем исходное изображение в 2 раза по двум осям.

Первым делом давайте вычислим координаты углов изображения после преобразования:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/c5a4821acb200f5ebae32ad1ac01655fa2a2de69/sample_scale_results.png)
 
Посмотрите на реализацию:

~~~ java

package mercuriy94.com.matrix.affinetransformations;

//Импорты 
...

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                scale();
            }
        });
    }

  private void scale() {

        Matrix transformMatrix = new Matrix();

        float sx = 2f;
        float sy = 2f;

        float[] matrixValues = new float[]{
                sx, 0f, 0f,
                0f, sy, 0f,
                0f, 0f, 1f};

        transformMatrix.setValues(matrixValues);

        Matrix imageMatrix = imageView.getImageMatrix();

        imageMatrix.postConcat(transformMatrix);
        imageView.setImageMatrix(imageMatrix);

        printImageCoords(transformMatrix);
    }
  
  ...
    
}

~~~

Результат:

![](https://bitbucket.org/mercury94/articles/raw/21ea21153014172695b19b71e9e6cceae5b29874/image_sample_scale_result.png)

Убеждаемся в паравильности расчетов:

![](https://bitbucket.org/mercury94/articles/raw/648289e11fd7e7ea90a53cd82a08c99c3ec45fe5/matix_values_log_input_scale_coords.png)

Скорее всего, вы уже догадались, что нам необязательно создавать матрицу преобразования. Для этого нам достаточно знать о методах postScale(...) и preScale(...). Эти методы включают в себя обязательные параметры: sx и sy. А также опциональные (необезательные) параметры: px и py. 
Вот так выглядит использование метода scale(), без матрицы преобразования:

~~~ java

...

  private void scale() {

        float sx = 2f;
        float sy = 2f;

        Matrix imageMatrix = imageView.getImageMatrix();

        imageMatrix.postScale(sx, sy);
        imageView.setImageMatrix(imageMatrix);
    }
  
...
~~~

А теперь давайте рассмотрим пример поинтереснее. 
Масштабировать будем с опорной точкой в центре изображения. Но, чтобы все было красиво, давайте перед этим выполним известную нам операцию переноса в центр контейнера. Таким образом, у нас получится комбинация преобразований. 
Как и прежде, сначала рассчитаем финальное положение углов изображения. Но, чтобы это выполнить, нам необходимо получить опорную точку для масштабирования. Так как изображение будет находиться в центре контейнера, то опорной точкой будет центр контейнера, координаты которой равны половине его размеров. В моем случае получилось: 
px = 384;
py = 512;

Параметры  tx и ty равны 264 и 352 соответственно, мы их вычисляли в предыдщуем разделе. Теперь когда нам известны все переменные, можем перейти к вычислениям:

 ![](https://bitbucket.org/mercury94/articles/raw/cdb15790f15294bdcee25755ad767d1e66c3dbbf/image_formula_scale_sample_anchor_point.png)
 
А теперь напишем реализацию в андройде:

~~~ java

package mercuriy94.com.matrix.affinetransformations;

//imports
...
  
public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                transAndScale();
            }
        });
    }

 private void transAndScale() {

        Matrix transformImageMatrix = imageView.getImageMatrix();

        //region translate to center

        Matrix translateToCenterMatrix = new Matrix();

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        float[] translateMatrixValues = new float[]{
                1f, 0f, tx,
                0f, 1f, ty,
                0f, 0f, 1f};

        translateToCenterMatrix.setValues(translateMatrixValues);

        transformImageMatrix.postConcat(translateToCenterMatrix);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        float[] scaleMatrixValues = new float[]{
                sx, 0f, -px * sx + px,
                0f, sy, -py * sy + py,
                0f, 0f, 1f};

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setValues(scaleMatrixValues);

        transformImageMatrix.postConcat(scaleMatrix);

        //endregion scale

        imageView.setImageMatrix(transformImageMatrix);
   
        printMatrixValues(transformImageMatrix);
        printImageCoords(transformImageMatrix);
    }
  
  ....

}

~~~

Для удобства чтения я разбил метод на 2 региона, чтобы вы могли с легкостью наблюдать последовательное применение преобразования. Результат получился следующим:

 ![](https://bitbucket.org/mercury94/articles/raw/24dedb23fe9af71c8245c3a78316b24b9b4d0a0a/image_sample_scale_2_result.png)
 
 Вывод лога можно разбить на 2 части:
 1 -  Вывод получившейся матрицы преобразования:
 
  ![](https://bitbucket.org/mercury94/articles/raw/74f0391589c3453964a5c42498ec84b9e36ec461/matix_values_log_input_scale_trans.png)
 
 2 - Вывод коордиант изображения после преобразования:
 
  ![](https://bitbucket.org/mercury94/articles/raw/74f0391589c3453964a5c42498ec84b9e36ec461/matix_values_log_input_scale_trans_coords.png)

Выводы в лог, позволяют нам судить, что наши расчеты верны!

 Теперь перепишем метод transAndScale() с использованием методов postTranlate() и postScale():

~~~ java

...
  
  private void transAndScale() {

        Matrix transformImageMatrix = imageView.getImageMatrix();

        //region translate to center

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        transformImageMatrix.postTranslate(tx, ty);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        transformImageMatrix.postScale(sx, sy, px, py);

        //endregion scale

        imageView.setImageMatrix(transformImageMatrix);
    }

...
  
~~~


### Поворот (вращение) изображения  (Rotate)

Преобразование поворота устаналивает соответствие между координатами точки, объекта и экраном системы координат наблюдателя при вращении объекта (без сдвига) отностиельно начала координат.
То есть, мы будем вращать плоскость, на которой лежит наше изображение.
Прежде чем начать обсуждать вращение, договоримся, что принято считать вращение против часовй стрелки положительным. При таком раскладе, удобно считать, что угол поворота (альфа) лежит в интервале [-П;П].

Чтобы найти формулу преобразования координат, выберем произвольную точку(x,y), вектор которой обозначим r. Тут уже требуется освежить в памяти тригонометрию.

![](https://bitbucket.org/mercury94/articles/raw/c6692dc1ac6de6224e2bb2ff59ef8d1297c4f4bd/x%3Drcos.png)

![](https://bitbucket.org/mercury94/articles/raw/c6692dc1ac6de6224e2bb2ff59ef8d1297c4f4bd/y%3Drsin.png)

Если повернем на угол тета:

![](https://bitbucket.org/mercury94/articles/raw/2782d406809d3841b70a180ba865aded99819975/x'%3Drcos.png)

![](https://bitbucket.org/mercury94/articles/raw/2782d406809d3841b70a180ba865aded99819975/y'%3Drsin.png)

Зная формулы суммы углов для синуса и косинуса:

![](https://bitbucket.org/mercury94/articles/raw/359494d16437cda621b58d6f3e58d967ee007768/sin(a%2Bq).png)

![](https://bitbucket.org/mercury94/articles/raw/359494d16437cda621b58d6f3e58d967ee007768/cos(a%2Bq).png)

Тогда:

![](https://bitbucket.org/mercury94/articles/raw/19e09c403a2a2da89975c9786c6bd9ffc4360d9f/x'%3Drcos(a%2Bq).png)

![](https://bitbucket.org/mercury94/articles/raw/19e09c403a2a2da89975c9786c6bd9ffc4360d9f/y'%3Drsin(a%2Bq).png)

Раскроем скобки и выполним замены, получим:

![](https://bitbucket.org/mercury94/articles/raw/02a2a049b28933fec9b6215bed9e2d88b5fc50f6/x'%3Dmatrix_rotate_formula.png)

![](https://bitbucket.org/mercury94/articles/raw/02a2a049b28933fec9b6215bed9e2d88b5fc50f6/y'%3Dmatix_rotate_formula.png)

Теперь осталось найти матрицу преобразования:

![](https://bitbucket.org/mercury94/articles/raw/7b57c4dcb0ca1106bcfad262b029b591c90d4c83/matrix_rotate.png)

![](https://bitbucket.org/mercury94/articles/raw/1b1773bc57459bcb559622d895739956293f9ebb/matrix_rotate_final.png)


И получим матрицу 3 х 3, применяемую для преобразования врещения: 

![Alt Text](https://bitbucket.org/mercury94/articles/raw/7b9979eddf5b845a2a12b9c4fa4fb00a7b0b601a/matrix_rotate_final_formula.png)


Пример:
В качестве примера, оставим давно известные координаты из предыдущих примеров. Поворачивать будет на 90 градусов.

![](https://bitbucket.org/mercury94/articles/raw/e1d02918a0c6bfb14f4bff1b4d2e610bef689cb6/matrix_rotate_input_params.png)

Решение:

![](https://bitbucket.org/mercury94/articles/raw/e1d02918a0c6bfb14f4bff1b4d2e610bef689cb6/matrix_rotate_example.png)

![](https://bitbucket.org/mercury94/articles/raw/28bbeb0f6f892551217447af4186b10a09489a83/matrix_rotate_example_1.png)

На первый взгляд, результат может показаться странным, но давайте не будем забывать, что мы вращаем плоскость объекта. Чтобы вращать плоскость вокруг опорной точки (например центра изображения), необходимо выполнить следующие операции преобразования:
  1: Сместить на обратные координаты опорной точки;
  2: Выполнить вращение;
  3: Сместить обратно на координаты опорной точки;

Таким способом мы получим необходимую матрицу преобразования вокруг опорной точки:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/07175207dc95257399568f7932332e641c7b7e09/matrix_rotate_anchor_point_final.png)

Так как мы испольузем несколько разных типов преобразований, то этот вид вращения можно считать комбинацией преобразований.  

В качестве примера, входые данные оставим из предыдущего примера, только опорной точкой выберем центр квадрата. Координаты опорной точки получаются P(5,5).

![](https://bitbucket.org/mercury94/articles/raw/784abd7652c0de5766392b87f1d15f37f66bc1cc/matrix_rotate_anchor_point_example_final.png)

Результат:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/b402f10f0f42a41d66b48e48c2a0b115a309708e/matrix_rotate_example_2.png)

Теперь рассмотрим применение вращения в Android.
Предлагаю опустить базовый пример состоящего из одного преобразования, вместо этого предлагаю дополнить пример из раздела "Масштабирование". Следовательно в качестве исходных данных мы имем отцентрированное и растянутое по двух осям изображение, тогда в качестве исходной матрицы мы возьмем результат матрицы из раздела "Машстабирование". Вращать будем на 90 градусов.
Как обычно, давайте рассчитаем координаты на которые лягут углы изображения.

P.S: Предвижу ваши ужасания, но вам необязательно прослеживать весь путь решения, достаточно понимать выполняемые действия :=) 
 
 ![](https://bitbucket.org/mercury94/articles/raw/1351698066b8974389943da139749c5ae450779b/formua_sample_image_rotate_anchor_point%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.png)
 
 Теперь, наконец-то, давайте перейдем к коду:
 
~~~ java

package mercuriy94.com.matrix.affinetransformations;

//Импорты
...

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                transScaleRotate();
            }
        });
    }

     private void transScaleRotate() {

        Matrix transformImageMatrix = imageView.getImageMatrix();

        //region translate to center

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        transformImageMatrix.postTranslate(tx, ty);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        transformImageMatrix.postScale(sx, sy, px, py);

        //endregion scale

        //region rotate

        Matrix matrixRotate = new Matrix();

        double degrees = Math.toRadians(90d);

        float[] rotateMatrixValues = new float[]{
                (float) Math.cos(degrees), -(float) Math.sin(degrees), -(float) Math.cos(degrees) * px + (float) Math.sin(degrees) * py + px,
                (float) Math.sin(degrees), (float) Math.cos(degrees), -(float) Math.sin(degrees) * px - (float) Math.cos(degrees) * py + py,
                0f, 0f, 1f};


        matrixRotate.setValues(rotateMatrixValues);
        transformImageMatrix.postConcat(matrixRotate);

        //endregion rotate

        imageView.setImageMatrix(transformImageMatrix);
       
        printMatrixValues(transformImageMatrix);
        printImageCoords(transformImageMatrix);
    }
}

~~~

Я переименовал метод в transScaleRotate(). Операции сдвига и масштабирования я оставил не изменными, но добавил регион кода выполняющего вращение (rotate). 

 ![](https://bitbucket.org/mercury94/articles/raw/09a9fe4fdbb4e4e98e5f80acbd0e94b1ca89427a/image_sample_rotate.png)
 
 Выводы в лог подтвреждают правильность наших расчетов:
 
  ![](https://bitbucket.org/mercury94/articles/raw/6e725d4f24d5b9fbdd118e2d42f25b3eb4591f98/matix_values_log_input_transScaleRotate.png)

  ![](https://bitbucket.org/mercury94/articles/raw/6e725d4f24d5b9fbdd118e2d42f25b3eb4591f98/matix_values_log_input_transScaleRotate_coords.png)
 
  Конечно, класс Matrix включает в себя уже готовыe методы для выполнения вращения: postRotate и preRotate. У данных методов есть обязательный параметр degrees, который определяет угол поворота плоскости изображения в градусах. А также опциональные параметры px и py, также как и в масштабирование эти параметры определяют координату опорной точки. А теперь, по традиции, перепишем метод transScaleRotate с использованием метода postRotate(). 
Обратите внимание, что в операциях масштабирования и вращения мы используем одни и те же значения px и py, так как координаты опорных точек для этих опреаций вы выбрали одинаковыми, что является центром контейнера ImageView.

~~~ java

...

  private void transScaleRotate() {

        Matrix imageMatrix = imageView.getImageMatrix();

        //region translate to center

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        imageMatrix.postTranslate(tx, ty);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        imageMatrix.postScale(sx, sy, px, py);

        //endregion scale

        //region rotate

        float degrees = 90f;

        imageMatrix.postRotate(degrees, px, py);

        //endregion rotate

        printMatrixValues(imageMatrix);

        imageView.setImageMatrix(imageMatrix);
    }

...

~~~

### Наклон (Skew)
К сожалению, я не нашел понятного объяснения данного преобразования. Так что, будем получать матрицу преобразования опытным путем. Для начала, взгялнем на следующее изображение.

 ![Alt Text](https://bitbucket.org/mercury94/articles/raw/3e94b24cdfc27b6da93cdd1a5cb7d96a976d85a6/matrix_skew_image.png)

Здесь параллелограмм с зеленым цветом периметра является квадратом, подвергнутым наклону. Мы можем провести касательную левой-верхней точки границы наклоненного квадрата. Таким образом, мы получим угол наклона. 

А также, наблюдаем прямоугольный трегоульник (B'CC'). Вспоминаем, что в прямоуголном треугольнике:
 - Гипотенуза — самая длиная сторона;
 - Катеты — стороны, лежащие напротив острых углов;
 - Противолежащий катет — сторона треугольник которая лежит напротив заданного острого угла (СС');
 - Прилежащий катет — сторона треуголника которая прилегает к заданному острому углу (B'C);

Основываясь на курсе тригонометрии, мы можем воспользоваться следующими тождествами:

Синус острого угла в прямоугольном треугольнике — это отношение противолежащего катета к гипотенузе:

 ![Alt Text](https://bitbucket.org/mercury94/articles/raw/c2382b4b566c5b2022d077f47590c337fbe2ecc6/sinA.png)

Косинус острого угла в прямоугольном треугольнике — отношение прилежащего катета к гипотенузе:

 ![Alt Text](https://bitbucket.org/mercury94/articles/raw/c2382b4b566c5b2022d077f47590c337fbe2ecc6/cosA.png)

Тангенс острого угла в прямоугольном треугольнике — отношение противолежащего катета к прилежащему:

 ![Alt Text](https://bitbucket.org/mercury94/articles/raw/c2382b4b566c5b2022d077f47590c337fbe2ecc6/tanA.png)

Так как, противолежащая сторона нам неизвестна, но мы знаем угол наклона и прилежащий катет, то мы с легкостью можем воспользоваться функцией тангенса:

 ![Alt Text](https://bitbucket.org/mercury94/articles/raw/c2382b4b566c5b2022d077f47590c337fbe2ecc6/CC'.png)

Теперь нам остается решить систему уравнений, чтобы найти матрицу преобразования  для наклона:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/8a963f0a811757c28dc21f429690d24a78e65c72/y%3Dy'.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/0f15c1c72e5f38e8f95f8e8c1fadf3f2e52c3512/matrix_x_skew.png)

Мы также помним общую формулу преобразования для оси абцисс.

![Alt Text](https://bitbucket.org/mercury94/articles/raw/ab9cf66a69e9f6191d432875dd97707453bdca51/x'%3Dax%2Bby%2Bcz.png)

![Alt Text](https://bitbucket.org/mercury94/articles/raw/a8a45e424fca1ae44930547dd622eabf50eb4256/a%3D1.png)

Аналогичная ситуация и с осью ординат. В итоге мы получаем матрицу преобразования 3x3 для наклона по оси абцисc (X):

![Alt Text](https://bitbucket.org/mercury94/articles/raw/90c0d7c2dc3aa0574490bc3fc10c385a0ce6a5fb/matrix_skew_X.png)

А для оси ординат (Y):

![Alt Text](https://bitbucket.org/mercury94/articles/raw/90c0d7c2dc3aa0574490bc3fc10c385a0ce6a5fb/matrix_skew_Y.png) 

А если вам понядобиться одновременно наклонять по двум осям, то используем полную матрицу:

![Alt Text](https://bitbucket.org/mercury94/articles/raw/be7b53252ebd3fe27e7cd198f88149ba3ebf88de/matrix_skew_formula.png) 

Отлично, но ведь можно еще наклонять вокруг опорной точки, тут как и в предыдущих разделах не обойтись без сдвигов:

  1: Сместить на обратные координаты опорной точки;
  2: Выполнить вращение;
  3: Сместить обратно на координаты опорной точки;

![](https://bitbucket.org/mercury94/articles/raw/3db7802c34ed6961e1301bba631282766957614a/matrix_skew_anchor_point_formula.png) 

Не перепутайте, что угол альфа используется для наклона по оси абцисс, а бета по оси ординат.

В качестве примера, давайте выполним наклон отцентрированного и растянутого изображения. Исходной матрицей предлагаю взять результат матрицы из раздела "Масштабирование". Наклонять будем вокруг опорной точки, как обычно, в качестве координаты опорной точки возьмем центр контейнера ImageView. Выполним наклон по абцисс на 15 градусов, а по оси ординат на 30 градусов. 

 ![](https://bitbucket.org/mercury94/articles/raw/0dfa91b51b628a2d86a5d06e7b9abc4357dcdfc3/%20image_skew_sample.png)
  
  Перепишем наш пример:
  
~~~ java

package mercuriy94.com.matrix.affinetransformations;

//  Импорты
...

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                transScaleSkew();
            }
        });
    }

  private void transScaleSkew() {

        Matrix transformImageMatrix = imageView.getImageMatrix();

        //region translate to center

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        transformImageMatrix.postTranslate(tx, ty);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        transformImageMatrix.postScale(sx, sy, px, py);

        //endregion scale

        //region skew

        float[] skewMatrixValues = new float[]{
                1f, (float) Math.tan(Math.toRadians(15d)), -(float) Math.tan(Math.toRadians(15d)) * py,
                (float) Math.tan(Math.toRadians(30d)), 1f, -(float) Math.tan(Math.toRadians(30d)) * px,
                0f, 0f, 1f};

        Matrix skewMatrix = new Matrix();
        skewMatrix.setValues(skewMatrixValues);

        transformImageMatrix.postConcat(skewMatrix);

        //endregion skew

        imageView.setImageMatrix(transformImageMatrix);

        printMatrixValues(transformImageMatrix);
        printImageCoords(transformImageMatrix);
    }

  ...
    
}

~~~

Результат получился следующим:

 ![](https://bitbucket.org/mercury94/articles/raw/f9b49b0981c793d04b29b83e5938d2949a07b3ea/image_skew_sample_result.png)
 
 Выводы в лог позволяют нам заключить, что наши расчеты вполне сносны:
 
  ![](https://bitbucket.org/mercury94/articles/raw/207e58ab7ce1a823142a1d8a0c81d03d939ccb03/matix_values_log_input_transScaleSkew.png)

 ![](https://bitbucket.org/mercury94/articles/raw/207e58ab7ce1a823142a1d8a0c81d03d939ccb03/matix_values_log_input_transScaleSkew_coords.png)

Конечно, класс Matrix содержит готовые методы для выполнения наклона postSkew и preSkew. У этих методов есть два обязательных параметра kx и ky, которые являются значениями тангенса угла. Если вам нужно наклонять только по одной из осей, то во второй параметр передаем 0f, так как тангенс нуля равен нулю. А также, имеется два опциональных параметра px и py, смысл которых нам уже известен. Теперь перепишем наш метод transScaleSkew с использованием метода postSkew.

~~~java

...

  private void transScaleSkew() {

        Matrix imageMatrix = imageView.getImageMatrix();

        //region translate to center

        float tx = (imageView.getMeasuredWidth() - imageView.getDrawable().getIntrinsicWidth()) / 2f;
        float ty = (imageView.getMeasuredHeight() - imageView.getDrawable().getIntrinsicHeight()) / 2f;

        imageMatrix.postTranslate(tx, ty);

        //endregion translate to center

        //region scale

        float sx = 2f;
        float sy = 2f;

        float px = imageView.getMeasuredWidth() / 2f;
        float py = imageView.getMeasuredHeight() / 2f;

        imageMatrix.postScale(sx, sy, px, py);

        //endregion scale

        //region skew

        imageMatrix.postSkew((float) Math.tan(Math.toRadians(15)), (float) Math.tan(Math.toRadians(30)), px, py);

        //endregion skew

        printMatrixValues(imageMatrix);

        imageView.setImageMatrix(imageMatrix);
    }
...

~~~

### Итоги

Давайте подведем итог и вспомним, какие матрицы преобразования мы рассмотрели:

Параллельный перенос (сдвиг):

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_translation.png)

Масштабирование:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_scale%202.png)

Масштабирование с опорной точкой:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_scale_anchor_point.png)

Вращение:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_rotate%202.png)

Вращение с опорной точкой:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_rotate_anchor_point.png)

Наклон:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_skew.png)

Наклон с опорной точкой:

 ![](https://bitbucket.org/mercury94/articles/raw/a8b098cc442c2502660f34362044ce5983b00b55/matrix_skew_anchor_point.png)


Теперь мы знаем, как применять аффинные преобразования на практике и рассчитывать координату на которую ляжет та или иная точка в результате преобразований. Конечно, чаще всего вам не придется выполнять все эти операции, но знать, по каким правилам изменяется положение точек, необходимо.

Всем чистого кода!